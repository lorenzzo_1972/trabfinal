# Criação de um aplicativo utilizando Android Studio.


 A funcionalidade principal do aplicativo é comunicação com base de dados Firebird, que hoje é utilizada em um sistema feito em Delphi que está em produção.



# Funcionalidades

 Ao abrir ele se conecta com o servidor e atualiza o cadastro de marcas, cores, classe e tabela de preços na base local.

  
 #### Situação 1 - Se o carro já está cadastrado na base
 
 O usuário digita a placa do veículo e o sistema somente traz os dados (marca, cor, classe e ultima tabela de preço usada).
 
 #### Situação 2 -  Se o carro não está cadastrado na base 
 
 O usuário digita a placa do veículo, informa a marca, a cor, a classe e tabela de preço.
 
 #### Comportamento em ambas situações acima citadas
 
 O usuário só precisa confirmar inserção do veículo. 
 De forma automática é disparada uma flag pro servidor para que seja impresso o ticket de entrada.
 
 